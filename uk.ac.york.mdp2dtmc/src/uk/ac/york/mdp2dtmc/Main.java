package uk.ac.york.mdp2dtmc;

public class Main {

	public static void main(String[] args) throws Exception {

		//System.out.println("! syntax: java -jar <this jar> <model path> <prop path>");
		if(args.length<1)
			System.out.println("Error, you should have two files: model and property files\n syntax: java -jar <this jar> <model path> <prop path> ");
		String model = args[0];
		String prop = args[1];
		//model="inputs/firewire_abst/firewire_abst.nm";
		//prop="inputs/firewire_abst/firewire_abst.pctl";
		MDPTransform transformer = new MDPTransform(model, prop);
		transformer.run();
		
		System.exit(0);
	}
}
