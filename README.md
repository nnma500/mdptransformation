The aim of this project is to transform a given Markov Decision Process (MDP) model into a parametric discrete-time Markov chain (pDTMC), then employ a search-based technique with verification to produce a Pareto front for various optimal solutions.
* * *
### Requirements:

- PRISM model checker, which is required to generate the transition matrix of the Markov Decision Process (MDP) model and assists in the transformation process.

- EvoChecker: It is a tool that combines verification and optimisation of different probabilistic models. The output of the transformation approach will be used as input for the EvoChecker to find the optimal solutions or their approximations. EvoChecker is available at: https://github.com/gerasimou/EvoChecker.

- Other requirements: Python3 and Panda library to plot the Pareto diagram.
* * *
### Instructions:

1. **To convert a given MDP model into a pDTMC, run the following command:**

_java -jar MDPtransformation.jar MDP_model_file MDP_property_file_

As a result of this command, you will get two files in the form of pDTMC: “output_model.pm” and “output_props.pctl”.
 
In some cases, one of the properties cannot be converted automatically. Therefore, you could manually convert this part by exploiting the label feature in PRISM and using our jar file "readLabel" to convert this part if possible. 
For example, Pmin=? [ !"collision_max_backoff" U "all_delivered" ] in the CSMA model.  


2. **Use the files produced from the previous step as inputs to EVoChecker, then make the required configurations to obtain the Pareto-optimal results.**

When EvoChecker is imported into your preferred IDE (e.g., Eclipse), follow the next instructions to get the optimal results:
- (a) Go to the folder called "models", create a subfolder with any name (e.g. "MDPtransform") and put the files generated from Step 1 inside this folder.
- (b) Go to the file called “config.properties”. You will find 6 steps. Complete these steps as follows.
    - In step 1, give the problem a name,  and let “Model_TEMPLATE_FILE” and “PROPERTIES_FILE” indicate the files in step (a).
    - In step 2, set the preferred multi-objective genetic algorithms (MOGA) to run. We chose NSGII.
    - In steps 3 and 4, set the required population size and evaluation size. We set each one equal to 100.
    - In step 5, for the parallel execution, set the number of processors if your device has more than one processor. Otherwise, keep it equal to 1. In our case, we set it equal to 6.
    - In step 6, set the settings to plot the Pareto diagram. This step requires Python, and you should have more than two objectives; otherwise, keep it false.
    - In step 7, set VERBOSE = true if you want to see the results on the console.
![Semantic description of image](fig/evocheckerSettings.png)*Figure 1: Screenshot of EvoChecker's Configuration file*

- (c) The results will be stored in a folder called “data”.


Note: for the first part of experiment, just remove the properties that you do not want to obtain their results, and disable the generation of Pareto figure from EvoChecker’s configuration file.  For the second part, keep all properties and enable the generation of Pareto figure.
* * *
### Manual transformation:

For a property/reward that cannot be transformed by our tool, follow these instructions to convert it manually:

- In PRISM, export the label.
- Run the following command, and follow the instructions on the terminal:
_java -jar label.jar label_name_
- Copy the output, and paste it into the model/property file.

Example:
We want to get the values of the two labels, "collision_max_backoff" and "all_delivered”, found in the following property: 

Pmin=? [ !"collision_max_backoff" U "all_delivered" ];

First, we should put the MDP model in PRISM, then export the label file as a text file and give it a name (e.g. Mlabel.txt).
![Semantic description of image](fig/exportLabel.png)*Figure 2: Exporting label for MDP model as plain text file*

 Next, run the following command, and follow the instructions that appear in the terminal:

_java -jar Mlabel.txt_

The following picture show how we get the value of these labels: 
![Semantic description of image](fig/UsingReadjar.png)*Figure 3: Converting property using readLable.jar file*

Then, replace the labels with their values in the property. After updating, the property will be:

Pmin=? [ ! (u=1034|u=1037 ) U (u=114|u=128|u=129 )];

* * *
### PRISM Installation:

To install PRISM, follow the instructions at this link: http://www.prismmodelchecker.org/manual/InstallingPRISM/Instructions.

To get a verification result for a property of a model, follow these steps:

- Copy and paste the model into the model part of the PRISM GUI.
![Semantic description of image](fig/InsertModelPrism.png)*Figure 4: Inserting a model into PRISM GUI*

- Copy and paste the properties into the property part of the PRISM GUI.
- Select the property we want to verify, and choose to verify with a right click.
![Semantic description of image](fig/InsertPropertyPrism.png)*Figure 5: Verifying a property using PRISM*


