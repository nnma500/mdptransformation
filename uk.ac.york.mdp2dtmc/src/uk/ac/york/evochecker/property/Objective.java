//form EvoChecker project
package uk.ac.york.evochecker.property;

public class Objective extends Property {

	/**
	 * Construct a new objective QoS property.
	 * 
	 */
	public Objective() {
		super();
	}

	/**
	 * Construct a new objective QoS property.
	 * 
	 * @param maximization
	 */
	public Objective(boolean maximization) {
		super(maximization);
	}
}
